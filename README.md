# A statically generated blog example using Next.js and ButterCMS

This example showcases Next.js's [Static Generation](https://nextjs.org/docs/basic-features/pages) feature using [ButterCMS](https://buttercms.com/) as the data source.

## Demo

[https://butter-cms-blog.vercel.app/](https://butter-cms-blog.vercel.app/)

## Configuration

### Run Next.js in development mode

```bash
npm install
npm run dev

# or

yarn install
yarn dev
```

The blog should be up and running on [http://localhost:3000](http://localhost:3000)!