import Butter from "buttercms";

const butter = Butter(process.env.BUTTER_CMS_API_TOKEN);
const postsPageSize = 10;

export const getPost = async(slug) => {
  const post = await butter.post.retrieve(slug);
  return post?.data?.data;
}

export const getPostsData = async(page = 1, pageSize = postsPageSize) => {
  const response = await butter.post.list({
    page_size: pageSize,
    page: page,
  });

  return {
    posts: response?.data?.data,
    prevPage: response?.data?.meta?.previous_page,
    nextPage: response?.data?.meta?.next_page,
  };
}

export const getAllPostsPaginated = async(pageSize = postsPageSize) => {
  const paginatedPosts = [];
  let currentPage = 1;
  while (!!currentPage) {
    const pagePostsData = await getPostsData(currentPage, pageSize);
    paginatedPosts[currentPage - 1] = pagePostsData.posts;
    currentPage = pagePostsData.nextPage;
  }
  return paginatedPosts;
}

export const getPagesByType = async(type) => {
  const pages = await butter.page.list(type);
  return pages?.data?.data;
}



