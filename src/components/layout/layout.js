import { useEffect, useState } from 'react';

import { Content, Layout as AntLayout } from 'src/components/uielements/layout';
import Header from 'src/components/layout/header';
import { Row, Col } from 'src/components/uielements/grid';

const Layout = ({ children }) => {
  const [isLayoutReady, setIsLayoutReady] = useState(false);

  useEffect(() => {
    setIsLayoutReady(true);
  }, [])

  return isLayoutReady ? (
    <AntLayout>
      <Header />
      <Content className="master-content">
        <Row gutter={ 16 }>
          <Col span={ 24 }>
            { children }
          </Col>
        </Row>
      </Content>
    </AntLayout>
  ) : null;
}

export default Layout;