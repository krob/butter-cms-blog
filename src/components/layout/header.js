import { useState, useEffect } from 'react';
import Link from "next/link";
import { useRouter } from 'next/router';

import { Header as AntHeader } from 'src/components/uielements/layout';
import Menu from 'src/components/uielements/menu';

const menuItems = [{
  title: "Home",
  href: "/"
}, {
  title: "Blog",
  href: "/blog"
}]

const Header = () => {
  const router = useRouter();
  const currentRoute = router.asPath;
  const [isReady, setIsReady] = useState(false);
  

  useEffect(() => {
    setIsReady(true);
  }, [])

  const activeRoute = () => {
    const activeIndex = menuItems.findIndex(menuItem => menuItem.href === currentRoute);
    return `menu-item-${activeIndex || 0}`;
  }

  const renderMenuItems = () => {
    return menuItems.map((menuItem, index) => (
      <Menu.Item key={ `menu-item-${index}` }>
        <Link href={ menuItem.href }>
          <a>{ menuItem.title }</a>
        </Link>
      </Menu.Item>
    ));
  }

  return !isReady ? null : (
    <AntHeader className="master-header">
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={[activeRoute()]}
      >
        { renderMenuItems() }
      </Menu>
    </AntHeader>
  )
}

export default Header;