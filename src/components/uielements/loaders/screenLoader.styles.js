import styled from 'styled-components';

const ScreenLoaderStyleWrapper = styled.div`
  .screen-loader {
    background-color: rgba(255,255,255,0.5);
    width: 100%;
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    z-index: 100000;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    opacity: 1;
    text-align: center;
    .wrapper {
      width: 100px;
      height: 100px;
      display: -ms-inline-flexbox;
      display: inline-flex;
      -ms-flex-direction: column;
      flex-direction: column;
      -ms-flex-pack: distribute;
      justify-content: space-around;
      .inner {
        animation: spinner .6s linear infinite;
        width: 40px;
        height: 40px;
        margin: 0 auto;
        text-indent: -12345px;
        border: 1px solid rgba(0,0,0,.08);
        border-left-color: rgba(0,0,0,.7);
        border-radius: 50%;
        z-index: 100001;
      }
    }
  }

  @-ms-keyframes spinner { 
    from { 
        -ms-transform: rotate(0deg); 
    } to { 
        -ms-transform: rotate(360deg); 
    }
  }
  @-moz-keyframes spinner { 
    from { 
        -moz-transform: rotate(0deg); 
    } to { 
        -moz-transform: rotate(360deg); 
    }
  }
  @-webkit-keyframes spinner { 
    from { 
        -webkit-transform: rotate(0deg); 
    } to { 
        -webkit-transform: rotate(360deg); 
    }
  }
  @keyframes spinner { 
    from { 
        transform: rotate(0deg); 
    } to { 
        transform: rotate(360deg); 
    }
  }
`;

export default ScreenLoaderStyleWrapper;
