import ScreenLoaderWrapper from 'src/components/uielements/loaders/screenLoader.styles';

const ScreenLoader = (props) => {
  const { message } = props;
  const renderLoaderMessage = () => {
    return (
      <div className="text">{ message }</div>
    );
  }

  return (
    <ScreenLoaderWrapper>
      <div className="screen-loader">
        <div className="wrapper">
          <div className="inner"></div>
          { renderLoaderMessage() }
        </div>
      </div>
    </ScreenLoaderWrapper>
  )
}

ScreenLoader.defaultProps = {
  message: 'Loading...'
}

export default ScreenLoader;