import { Layout, Menu, Breadcrumb } from 'antd';

import { AntContent, AntHeader } from 'src/components/uielements/layout/layout.styles';

const { Header, Footer, Content } = Layout;
const content = AntContent(Content);
const header = AntHeader(Header);

export { header as Header, Footer, content as Content, Layout };