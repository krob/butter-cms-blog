import styled from 'styled-components';

const AntContent = ComponentName => styled(ComponentName)`
  &.master-content {
    padding: 0 50px;
    margin-top: 64px;
  }
`;

const AntHeader = ComponentName => styled(ComponentName)`
  &.master-header {
    position: fixed;
    zIndex: 1;
    width: 100%;
  }
`;

export { AntContent, AntHeader };