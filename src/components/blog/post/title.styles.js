import styled from 'styled-components';

const BlogPostTitleStyleWrapper = styled.div`
  .blog-post-title {
    z-index: 101;
    position: relative;
    .ant-page-header-heading-title, .ant-page-header-heading-sub-title, .ant-page-header-back-button {
      color: #fff;
    }
  }
`;

export default BlogPostTitleStyleWrapper;
