import styled from 'styled-components';

const BlogPostContentStyleWrapper = styled.div`
  img {
    max-width: 100%;
  }
  .blog-post-content {
    padding: 50px 50px;
    background-color: #fff;
    margin-top: -50px;
  }
`;

export default BlogPostContentStyleWrapper;
