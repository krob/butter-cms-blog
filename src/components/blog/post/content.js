import { Row, Col } from 'src/components/uielements/grid'
import BlogPostContentStyleWrapper from 'src/components/blog/post/content.styles';

const BlogContent = (props) => {
  const { content } = props;
  
  return (
    <BlogPostContentStyleWrapper>
      <Row>
        <Col span={ 22 } offset={ 1 }>
          <div className="blog-post-content" dangerouslySetInnerHTML={{ __html: content }} />
        </Col>
      </Row>
    </BlogPostContentStyleWrapper>
  );
}

export default BlogContent;