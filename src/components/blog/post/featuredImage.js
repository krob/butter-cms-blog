import FeaturedImageStyleWrapper from 'src/components/blog/post/featuredImage.styles'
import PostTitle from 'src/components/blog/post/title';

const BlogPostFeaturedImage = (props) => {
  const { featuredImage, title, author, className } = props;

  const renderFeaturedImage = () => {
    return featuredImage ? (
      <div className={ `featured-image ${className}` } style={{ backgroundImage: `url(${featuredImage})` }} />
    ) : null;
  }

  const renderPostTitle = () => {
    return (
      <PostTitle
        title={ title }
        author={ author }
      />
    );
  }

  return (
    <FeaturedImageStyleWrapper>
      <div className="featured-image-wrapper">
        { renderFeaturedImage() }
        { renderPostTitle() }
      </div>
    </FeaturedImageStyleWrapper>
  )
}

BlogPostFeaturedImage.defaultProps = {
  className: "",
  title: null,
  featuredImage: null,
  author: {}
}

export default BlogPostFeaturedImage;