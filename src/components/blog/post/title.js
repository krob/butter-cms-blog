import { useRouter } from 'next/router';

import PageHeader from 'src/components/uielements/pageHeader';
import BlogPostTitleStyleWrapper from 'src/components/blog/post/title.styles';

const PostTitle = (props) => {
  const { title, author } = props;
  const router = useRouter();

  const renderAuthor = () => {
    const authorName = [author?.first_name, author?.last_name].filter(author => author).join(" ")
    return authorName ? (
      <small>by { authorName } </small>
    ) : null;
  }

  return (
    <BlogPostTitleStyleWrapper>
      <PageHeader
        className="blog-post-title"
        onBack={() => router.push('/blog')}
        title={ title }
        subTitle={ renderAuthor() }
      />
    </BlogPostTitleStyleWrapper>
  )
}

export default PostTitle;