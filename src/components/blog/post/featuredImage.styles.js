import styled from 'styled-components';

const FeaturedImageStyleWrapper = styled.div`
  .featured-image-wrapper {
    padding: 200px 0px;
    position: relative;
    &:after {
      position: absolute;
      background: rgba(0, 0, 0, 0.5);
      width: 100%;
      height: 100%;
      content: "";
      top: 0;
    }
    .featured-image {
      position: absolute;
      top: 0;
      width: 100%;
      height: 100%;
      background-position: center center;
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-size: cover;
      &.blog-card-image {
        background-attachment: initial;
      }
    }
  }
`;

export default FeaturedImageStyleWrapper;
