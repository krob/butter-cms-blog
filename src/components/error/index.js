import Link from "next/link";

import Result from 'src/components/uielements/result';
import Button from 'src/components/uielements/button';
import httpStatusCodes from 'src/data/httpStatusCodes';

const Error = (props) => {
  const { statusCode } = props;
  const renderBackToHome = () => (
    <Link href="/">
      <Button type="primary">Back Home</Button>
    </Link>
  );

  return (
    <Result
      status={ statusCode }
      title={ statusCode }
      subTitle={ httpStatusCodes[`${statusCode}`] }
      extra={ renderBackToHome() }
    />
  )
}

export default Error;