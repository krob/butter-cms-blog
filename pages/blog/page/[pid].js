import { useEffect, useState } from 'react';
import Link from "next/link";

import Layout from 'src/components/layout/layout';
import { Row, Col } from 'src/components/uielements/grid';
import Card, { Meta } from 'src/components/uielements/card';
import { getAllPostsPaginated, getPostsData } from 'src/lib/api';
import BlogPostFeaturedImage from 'src/components/blog/post/featuredImage';
import Error from 'src/components/error';

const Blog = (props) => {
  const { posts } = props;
  const [isPageLoaded, setIsPageLoaded] = useState(false);

  useEffect(() => {
    setIsPageLoaded(true)
  }, [])

  const renderFeaturedImage = (post) => {
    return (
      <BlogPostFeaturedImage
        featuredImage={ post.featured_image }
        className="blog-card-image"
      />
    )
  }

  const renderPosts = () => {
    return !posts && isPageLoaded ? (
      <Error
        statusCode={ 404 }
      />
    ) : isPageLoaded && posts.map((post, index) => {
      const span = index > 0 ? {
        sm: 12,
        xs: 24
      } : {};

      return (
        <Col
          { ...span }
          key={ `blog-col-${index}` }
        >
          <Link
            href={ `/blog/${post.slug}` }
            key={ `blog-card-${index}` }
          >
            <a>
              <Card
                hoverable
                cover={ renderFeaturedImage(post) }
              >
                <Meta title={ post.title } description={ post.summary } />
              </Card>
            </a>
          </Link>
        </Col>
      )
    })
  }

  return (
    <Layout>
      <Row gutter={ [16, 16] }>
        { renderPosts() }
      </Row>
    </Layout>
  )
}

Blog.defaultProps = {
  posts: []
}

export async function getStaticProps({ params }) {
  const page = parseInt(params ? params.pid : 1, 10);
  const { posts, prevPage, nextPage } = await getPostsData(page);

  return {
    props: {
      posts,
      prevPage,
      nextPage
    },
  };
}

export async function getStaticPaths() {
  const allPosts = await getAllPostsPaginated();
  const paths = Object.keys(allPosts).map(
    (pageIndex) => `/blog/page/${parseInt(pageIndex, 10) + 1}`
  );

  return {
    paths,
    fallback: false,
  };
}

export default Blog;