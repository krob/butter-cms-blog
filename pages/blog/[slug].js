import { useRouter } from "next/router";

import { getAllPostsPaginated, getPost } from 'src/lib/api';
import Layout from 'src/components/layout/layout';
import BlogPostFeaturedImage from 'src/components/blog/post/featuredImage';
import BlogContent from 'src/components/blog/post/content';
import Error from 'src/components/error';

const BlogPost = (props) => {
  const { post } = props;
  const router = useRouter();

  return (!router.isFallback && !post?.slug) ? (
    <Error
      statusCode={ 404 }
    />
  ) : (
    <Layout>
      <BlogPostFeaturedImage
        featuredImage={ post.featured_image }
        title={ post.title }
        author={ post.author }
      />
      <BlogContent
        content={ post.body }
      />
    </Layout>
  )
}

export async function getStaticProps({ params }) {
  const post = await getPost(params.slug);

  return {
    props: {
      post,
    },
  };
}

export async function getStaticPaths() {
  const allPosts = await getAllPostsPaginated();
  const paths = Object.entries(allPosts).reduce((res, [pageIndex, posts]) => {
    const pagePaths = posts.map((post) => `/blog/${post.slug}`);
    return [...res, ...pagePaths];
  }, []);

  return {
    paths,
    fallback: false,
  };
}

export default BlogPost;