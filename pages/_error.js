import Error from 'src/components/error';

const ErrorPage = ({ statusCode }) => {
  return (
    <Error
      statusCode={ statusCode }
    />
  );
}

ErrorPage.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}

export default ErrorPage;