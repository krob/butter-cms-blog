import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import 'antd/dist/antd.css'

import ScreenLoader from 'src/components/uielements/loaders/screenLoader';

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [isRouteLoading, setIsRouteLoading] = useState(false)
  
  useEffect(() => {
    router.events.on('routeChangeStart', handleRouteChange)
    router.events.on('routeChangeComplete', handleRouteComplete)
    router.events.on('routeChangeError', handleRouteComplete)

    return () => {
      router.events.off('routeChangeStart', handleRouteChange)
      router.events.off('routeChangeComplete', handleRouteComplete)
      router.events.off('routeChangeError', handleRouteComplete)
    }
  }, [])

  const handleRouteChange = (url) => {
    setIsRouteLoading(true)
  }

  const handleRouteComplete = (url) => {
    setIsRouteLoading(false)
  }

  return (
    <>
      { isRouteLoading && <ScreenLoader /> }
      <Component {...pageProps} />
    </>
  )
}
export default MyApp