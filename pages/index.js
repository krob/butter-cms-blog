import Layout from 'src/components/layout/layout';
import { getPagesByType } from 'src/lib/api';
import Error from 'src/components/error';

const HomePage = (props) => {
  const { page } = props;
  const readme = page?.fields?.readme || null;

  const renderPageContent = () => {
    return !readme ? (
      <Error
        statusCode={ 404 }
      />
    ) : (
      <div dangerouslySetInnerHTML={{ __html: readme }} />
    );
  }

  return (
    <Layout>
      { renderPageContent() }
    </Layout>
  )
}

export async function getStaticProps({ params }) {
  const page = await getPagesByType('homepage')
  const homepage = page.length ? page[0] : {}
  return {
    props: { page: homepage }
  };
}


export default HomePage;